#!/bin/bash

set -e

packages=(\
    "atk" \
    "autoconf" \
    "automake" \
    "bc" \
    "bind-utils" \
    "bind9" \
    "bind9utils" \
    "binutils" \
    "bison" \
    "blas" \
    "bzip2-devel" \
    "cairo" \
    "cmake" \
    "diffutils" \
    "dnsutils" \
    "ethtool" \
    "file" \
    "flex" \
    "gdb" \
    "gdbm-devel" \
    "git" \
    "glibc-langpack-en" \
    "gtk2" \
    "gtk2-devel" \
    "hwloc" \
    "htop" \
    "iproute" \
    "iproute2" \
    "iputils" \
    "language-pack-en" \
    "less" \
    "libatk1.0-0" \
    "libblas-dev" \
    "libblas3" \
    "libbsd-dev" \
    "libbsd-devel" \
    "libcairo2" \
    "libcairo2-dev" \
    "libfabric" \
    "libfabric1" \
    "libffi-devel" \
    "libgdbm-dev" \
    "libglib2.0-dev" \
    "libgtk2.0-0" \
    "libgtk2.0-dev" \
    "libnl-3-200" \
    "libnl-3-dev" \
    "libnl3" \
    "libnsl-dev" \
    "libnsl-devel" \
    "libopenblas-dev" \
    "libssl-dev" \
    "libsqlite3-dev" \
    "libtool" \
    "libuuid-devel" \
    "libxml2" \
    "libxml2-dev" \
    "libxml2-devel" \
    "lsof" \
    "ltrace" \
    "make" \
    "nc" \
    "net-tools" \
    "netcat" \
    "openssl-devel" \
    "openssh-client" \
    "openssh-clients" \
    "openssh-server" \
    "pandoc" \
    "patch" \
    "pciutils" \
    "perl" \
    "perl-Data-Dumper" \
    "perl-devel" \
    "procps" \
    "procps-ng" \
    "psmisc" \
    "reptyr" \
    "ripgrep" \
    "strace" \
    "sqlite-devel" \
    "sudo" \
    "tcl" \
    "tcl-dev" \
    "tcl-devel" \
    "tcsh" \
    "tk" \
    "tk-dev" \
    "tk-devel" \
    "tmux" \
    "util-linux" \
    "util-linux-user" \
    "valgrind" \
    "vim" \
    "wget" \
    "which" \
    "wheel" \
    "zlib-devel" \
    "zlib1g-dev" \
    "zsh" \
)

# Sort packages alphabetically (Helps with dependency hunting)
sorted_packages=($(printf '%s\n' "${packages[@]}" | sort))

if [ "$1" = "in" ]; then
    compiler="intel"
else
    compiler="$1"
fi

# Scraped for possible future use
# if [ -f /etc/os-release ]; then
#     # Read the values from /etc/os-release file
#     while IFS='=' read -r key value; do
#         # Remove quotes if present in the value
#         value=$(echo "$value" | tr -d '"')
#         # Save the values to variables with the same name
#         case "$key" in
#             NAME) NAME="$value";;
#             VERSION_ID) VERSION_ID="$value";;
#             ID) ID="$value";;
#             ID_LIKE) ID_LIKE="$value";;
#         esac
#     done < /etc/os-release
# fi
. /etc/os-release
echo "NAME: $NAME"
echo "VERSION_ID: $VERSION_ID"
echo "ID: $ID"
echo "ID_LIKE: $ID_LIKE"

# Function: detect_package_manager
#
# Purpose:
#   Detects the package manager associated with the current operating system by examining the OS ID provided by 
#   the /etc/os-release file. It supports a variety of Linux distributions and provides a mechanism to customize 
#   or handle specific versions of distributions if needed.
#
# Arguments:
#   None. However, the function depends on global variables provided by /etc/os-release, primarily the $ID and $VERSION_ID.
#
# Workflow:
#   1. Validates the presence of the $ID variable which should have been sourced from /etc/os-release.
#   2. Based on the $ID value:
#      - debian, ubuntu: Returns "apt"
#      - fedora, rhel, rocky: Returns "dnf"
#      - centos: Modifies repository URLs for CentOS 8 and returns "yum"
#      - opensuse*: Returns "zypper" and handles specific permissions for openSUSE Tumbleweed (commented out).
#      - Any other ID: Exits with an error message indicating an unknown package manager.
#   3. If $ID is not set, an error message indicates the absence of /etc/os-release.
#
# Output:
#   - Prints the detected package manager's name, such as "apt", "dnf", "yum", or "zypper".
#   - In case of unsupported distributions, prints an error message and exits.
#
# Dependencies:
#   - This function depends on the /etc/os-release file being present and having the required information.
#   - Commands like `sed` are utilized for modifying configuration files, so they should be accessible.
#
# Note:
#   - If specific actions are needed for certain versions of a distribution, the $VERSION_ID variable can be used, as shown 
#     for CentOS 8.
#   - There are commented-out sections indicating potential steps for openSUSE Tumbleweed. Before using these steps, ensure their 
#     relevance to the use case.
#   - It's advised to call this function and capture its output to determine the package manager for later use in scripts.
detect_package_manager(){
    if [ -n "$ID"  ]; then
        case $ID in
            debian|ubuntu)
                echo "apt"
                ;;
            fedora|rhel|rocky)
                echo "dnf"
                ;;
            centos)
                if [[ "$VERSION_ID" == "8" ]]; then
                    # "Change the repo URL to point to vault.centos.org, from the official CentOS repos."
                    sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
                    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
                fi
                echo "yum"
                ;;
            opensuse*)
                echo "zypper"
                # https://github.com/openSUSE/docker-containers/issues/82#issuecomment-494432343
                # Fix :
                # sudo: pam_open_session: Permission denied 
                # sudo: policy plugin failed session initialization 
                # if [[ "$ID" == "opensuse-tumbleweed" ]]; then
                #     ls /etc/security/
                #     echo > /etc/security/limits.conf || true
                # fi
                ;;
            *)
                echo "Unknown PackageManager: $ID"
                exit 1
                ;;
        esac
    else
        echo "Not running a distribution with /etc/os-release available"
    fi
}


pkg_mgr=$(detect_package_manager)
echo "Package Manager: $pkg_mgr. ID: $ID"
update_command=""
install_command=""
case $pkg_mgr in
    apt)
        update_command="apt-get update"
        install_command="apt-get install -y"
        ;;
    dnf)
        update_command="dnf -y update"
        install_command="dnf -y install"
        if [ "$ID" = "rocky" ]; then
            $install_command scl-utils wget
        fi
        dnf groupinstall 'Development Tools' -y
        ;;
    zypper) 
        update_command="zypper refresh"
        install_command="zypper install -y"
        $install_command system-group-wheel wget sudo nano #https://unix.stackexchange.com/questions/733326/sles15-sp4-does-wheel-group-not-exists-anymore
        ;;
esac

# Function: update_and_install_compiler
#
# Purpose:
#   Updates the system's package database and attempts to install the required packages
#   for the provided compiler. The function provides feedback on which packages were installed successfully and 
#   which ones failed. It's designed to handle various compilers like gcc, clang, intel, and llvm.
#
# Arguments:
#   compiler (str): The name of the compiler for which packages are to be installed (e.g., "gcc", "clang", "intel", "llvm").
#
# Workflow:
#   1. Validates the presence of `update_command` and `install_command` on the system.
#   2. Updates the system's package database using the `update_command`.
#   3. Depending on the provided compiler name, it determines the list of packages that should be installed:
#      - gcc: Various gcc related packages and dependencies.
#      - clang: clang related packages and tools.
#      - intel: Intel compiler related packages (partially implemented for Ubuntu and openSUSE).
#      - llvm: LLVM and related packages.
#   4. Installs each package using the `install_command` and tracks the success or failure of the installation.
#   5. Provides feedback on the success/failure status of each package's installation.
#
# Output:
#   - Prints the success/failure status of the installation of each package.
#   - If all packages were successfully installed, a success message is printed.
#   - If any package installation failed, a list of successful and failed packages is displayed.
#
# Dependencies:
#   - `update_command` and `install_command` should be defined and accessible.
#   - wget, gnupg, cmake, and other package management or build tools depending on the compiler.
#
# Note:
#   - This function contains commented-out sections which indicate potential implementation steps for certain compilers 
#     and are provided for reference. Before using them, one should ensure their appropriateness for the use case.
#   - This function does not return any values but will exit the current script execution on critical errors.
#   - Ensure that the appropriate repositories and permissions are set up for the package installations.
update_and_install_compiler() {
    local compiler=$1

    if ! command -v $update_command &> /dev/null
    then
        echo "Command $update_command not found"
        exit 1
    fi

    if ! command -v $install_command &> /dev/null
    then
        echo "Command $install_command not found"
        exit 1
    fi

    $update_command

    local compiler_packages
    case $compiler in
    gcc) 
        compiler_packages=('gcc' 'gcc-gfortran' 'gcc-c++' 'build-essential' 'gfortran' 'g++' 'compat-libgfortran' 'gcc-fortran')
        ;;
    clang)
        compiler_packages=('clang' 'clang-tools-extra' 'lldb' 'lld')
        ;;
    intel)  # https://software.intel.com/content/www/us/en/develop/articles/installing-intel-free-libs-and-python-apt-repo.html
            # not implemented fully yet
        # compiler_packages=('intel-compiler-base' 'intel-compiler-pro' 'intel-mkl')
        # https://neelravi.com/post/intel-oneapi-install/
        if [[ "$ID" == "ubuntu" ]]; then
            # $update_command
            # $install_command wget gnupg cmake pkg-config build-essential
            # wget -O- https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB | gpg --dearmor | tee /usr/share/keyrings/oneapi-archive-keyring.gpg > /dev/null
            # echo "deb [signed-by=/usr/share/keyrings/oneapi-archive-keyring.gpg] https://apt.repos.intel.com/oneapi all main" | tee /etc/apt/sources.list.d/oneAPI.list
            # $update_command    
            # $install_command intel-hpckit
            # Use official base image instead of ubuntu:latest
            return 
        elif [[ "$ID" == "opensuse-tumbleweed" ]]; then
            $update_command
            zypper --non-interactive install cmake pkg-config
            zypper --non-interactive install pattern devel_C_C++
        fi
        ;;
    llvm) # https://bugs.rockylinux.org/view.php?id=958
        compiler_packages=('llvm' 'llvm-dev' 'llvm-devel' 'clang' 'libclang-dev' 'libclang-devel' 'llvm-libs' 'lld')
        #  install Flang (Fortran frontend)
        $install_command cmake make git
        ;;
    *)  
        echo "Unknown compiler: $compiler"
        exit 1
        ;;  
    esac

    local install_failures=()
    local install_successes=()
    for package in "${compiler_packages[@]}"; do    
        if ! $install_command $package; then
            echo "Failed to install $package"
            install_failures+=("$package")
        else
            echo "Successfully installed $package"
            install_successes+=("$package")
        fi
    done

    if [ ${#install_failures[@]} -eq 0 ]; then
        echo "All compiler packages installed successfully!"
    else
        echo "> List of the successfully installed packages:"
        printf '%s\n' "${install_successes[@]}" | sort | while IFS= read -r package; do
            echo "-- $package"
        done

        echo "> List of the failed compiler packages:"
        printf '%s\n' "${install_failures[@]}" | sort | while IFS= read -r package; do
            echo "-- $package"
        done
    fi
}

# Function: update_and_install_packages
#
# Purpose:
#   Updates the system's package database and attempts to install a list of provided packages.
#   After attempting to install all packages, this function provides feedback on which packages were installed successfully and 
#   which ones failed.
#
# Arguments:
#   packages (array): A list of package names to be installed.
#
# Workflow:
#   1. Updates the system's package database using the `update_command` (expected to be defined outside the function).
#   2. Iterates through the provided list of packages.
#   3. Tries to install each package using the `install_command` (expected to be defined outside the function).
#   4. Tracks packages that were successfully installed and packages that failed.
#   5. Provides feedback on the success/failure status of each package.
#
# Output:
#   - Prints the success/failure status of the installation of each package.
#   - If all packages were successfully installed, a success message is printed.
#   - If any package installation failed, a list of successful and failed packages is displayed.
#
# Dependencies:
#   - `update_command` and `install_command` should be defined and accessible.
#
# Note:
#   This function does not return any values but will exit the current script execution on critical errors.
update_and_install_packages() {
    local packages=("$@")  

    $update_command
    
    local install_failures=()
    local install_successes=()
    for package in "${packages[@]}"; do
        echo "package: $package"
        if ! $install_command $package; then
            echo "Failed to install $package"
            install_failures+=("$package")
        else
            echo "Successfully installed $package"
            install_successes+=("$package")
        fi
    done

    if [ ${#install_failures[@]} -eq 0 ]; then
        echo "All packages installed successfully!"
    else
        echo "> List of the successfully installed packages:"
        for package in "${install_successes[@]}"; do
            echo "-- $package"
        done
        echo "> List of the failed packages:"
        for package in "${install_failures[@]}"; do
            echo "-- $package"
        done
    fi
}

# Function: verify_compiler_installation
#
# Purpose:
#   This function checks and verifies the installation of various compilers like gcc, clang, intel, and llvm. 
#   It displays the version of the installed compiler for confirmation.
#
# Arguments:
#   compiler (str): The name of the compiler to be verified (e.g., "gcc", "clang", "intel", "llvm").
#
# Workflow:
#   1. Checks the provided compiler name.
#   2. For "gcc": Checks versions of both gcc and gfortran.
#   3. For "clang": Checks the version of clang.
#   4. For "intel": Sources the Intel oneAPI setvars script, appends it to the user's `.bashrc`, and then checks the version of `ifort`.
#   5. For "llvm": Checks the version of llvm using `llvm-config`.
#   6. If an unrecognized compiler name is provided, the function exits with an error.
#
# Output:
#   - Prints the version of the installed compiler or exits with an error if the compiler is not recognized.
#
# Dependencies:
#   - Expected compilers must be installed on the system.
#   - For Intel compiler: The Intel oneAPI setvars script should be present at the specified location.
#
# Note:
#   Ensure the compilers have been installed properly before verifying their installations.

# Below the function definitions, there's a brief usage of the functions. Before running, the following variables should be set:
#   - `update_command`: Command to update the system's package database.
#   - `install_command`: Command to install a package.
#   - `compiler`: The name of the compiler to be installed and verified.
#   - `sorted_packages`: A list of packages to be installed.
verify_compiler_installation() {
    echo "Verifying compiler installation: $1"
    if [[ $1 == "gcc" ]]; then
        gcc --version  
        gfortran --version # must be at least 10
    elif [[ $1 == "clang" ]]; then
        clang --version
    elif [[ $1 == "intel" ]]; then
        echo "source $HOME/intel/oneapi/setvars.sh" >> ~/.bashrc && source ~/.bashrc # must do the same for mpiuser AFTER it is created!
        cat ~/.bashrc | grep oneapi
        ifort -V
        # icc --version  # deprecated

    elif [[ $1 == "llvm" ]]; then
        llvm-config --version 
    else
        echo "$1 has not been implemented yet!"
        exit 1
    fi
}

echo "Update command: $update_command"
echo "Install command: $install_command"
echo "Compiler: $compiler"
echo "> Installing packages..."
update_and_install_compiler "$compiler"
verify_compiler_installation $compiler
update_and_install_packages "${sorted_packages[@]}"

pypath=""
python_version="3.10.12" 

# Function: install_python_from_source
#
# Purpose:
#   This function installs a specific version of Python from source. By default, 
#   it's optimized for Python version 3.10.12 since the system was tested using the 
#   3.10.x series. The function will also install python-dev and pip packages.
#
# Usage:
#   install_python_from_source <Python_Version>
#   Where <Python_Version> is the version string (e.g., "3.10.12").
#
# Arguments:
#   version (str): The desired version of Python to be installed.
#
# Workflow:
#   1. Determines the major and minor version of Python based on the version string.
#   2. Constructs the source URL for downloading the tarball containing the source code of the desired Python version.
#   3. Downloads the tarball.
#   4. Extracts the tarball and enters the extracted directory.
#   5. Configures the Python build with optimization flags.
#   6. Compiles the Python source using all available cores.
#   7. Installs the compiled Python version.
#   8. Verifies the installed Python version.
#   9. Cleans up the extracted and downloaded files.
#   10. Sets the path to the installed Python executable.
#
# Output:
#   - If successful, the function will install the specified version of Python and will print its version to confirm the installation.
#   - Log files for configuration, compilation, and installation are generated to track any issues.
#   - The variable `pypath` is set to the path of the installed Python executable.
#
# Note:
#   It's important to ensure that all the required build dependencies are already installed on the system before running this function.
#   Please ensure you have the necessary permissions to install software on the system.
#
# Example:
#   To install Python version 3.10.12 from source:
#   install_python_from_source "3.10.12"
#
# Dependencies:
#   - wget: For downloading Python source tarball.
#   - tar: For extracting the tarball.
#   - make, gcc, and other build tools: For compiling Python.
#   - nproc: For getting the number of available cores.
install_python_from_source() {
    local version="$1"
    local major_version=${version%%.*}
    local minor_version=${version%.*}
    local source_url="https://www.python.org/ftp/python/$version/Python-$version.tar.xz"
    local tarball_name="Python-$version.tar.xz"
    local folder_name="Python-${version}"

    wget -q "$source_url"
    tar -xf "$tarball_name"
    pushd "$folder_name"

    ./configure --enable-optimizations > python_configure.log 2>&1
    make -j $(nproc) > python_make.log 2>&1
    sudo make install > python_make_install.log 2>&1

    python${minor_version} --version

    # Cleanup
    popd
    rm -rf "${folder_name}*" "${tarball_name}"

    pypath="/usr/local/bin/python${minor_version}"
}


install_python_from_source "$python_version"
$pypath -m pip install asyncio cython==0.29.34 SCons
