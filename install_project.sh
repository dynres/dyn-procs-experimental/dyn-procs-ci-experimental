#!/bin/bash

cd /opt/hpc/build/dyn_procs_setup
source env_vars_docker.sh
SCRIPT_PATH="./install_docker.sh"
sudo sed -i 's|EXEC scons|EXEC python -m SCons|g' "$SCRIPT_PATH"

# Following error is caused by cython 3.0.0
# ERROR: 
# make[3]: Entering directory '/opt/hpc/build/openpmix/bindings/python'
# /bin/python ../../bindings/python/construct.py --src="../../include" --include-dir="../../include"
# PMIX_TOP_SRCDIR=/opt/hpc/build/openpmix PMIX_TOP_BUILDDIR=/opt/hpc/build/openpmix \
# 	/bin/python ../../bindings/python/setup.py build_ext --library-dirs="/opt/hpc/install/pmix/lib" --user
# warning: pmix_constants.pxd:366:19: '_PMIX_PSETOP_P1' redeclared 
# warning: pmix_constants.pxd:367:19: '_PMIX_PSETOP_P2' redeclared 
# warning: pmix_constants.pxd:368:19: '_PMIX_PSETOP_PREF_NAME' redeclared 
# warning: pmix_constants.pxd:369:19: '_PMIX_PSETOP_PRESULT' redeclared 
# 
# Error compiling Cython file:
# ------------------------------------------------------------
# ...
#             info = NULL
#             ninfo = 0
# 
#         # pass our hdlr switchyard to the API
#         with nogil:
#              rc = PMIx_Register_event_handler(codes, ncodes, info, ninfo, pyeventhandler, NULL, NULL)
#                                                                           ^
# ------------------------------------------------------------
# 
# pmix.pyx:1369:74: Cannot assign type 'void (size_t, pmix_status_t, const pmix_proc_t *, pmix_info_t *, size_t, pmix_info_t *, size_t, pmix_event_notification_cbfunc_fn_t, void *) except * nogil' to 'pmix_notification_fn_t'
# Manual solution:
# if [ "$1" == "openpmix" ]; then
#     echo "$1"
#     echo "> HACK: openpmix"
#     echo " pmix_event_notification_cbfunc_fn_t cbfunc, void \*cbdata) with gil -> pmix_event_notification_cbfunc_fn_t cbfunc, void \*cbdata) nogil"
#     sed -i -e "s/pmix_event_notification_cbfunc_fn_t cbfunc, void \*cbdata) with gil:/pmix_event_notification_cbfunc_fn_t cbfunc, void \*cbdata) nogil:/g" /opt/hpc/local/build/openpmix/bindings/python/pmix.pyx
# fi

# awk '
# BEGIN { print_it=0 } 
# /EXEC cd "\$DYNMPI_BASE\/build\/openpmix"/ { print_it=1 } 
# { print }
# print_it == 1 { 
#     print "    sed -i -e \"s/pmix_event_notification_cbfunc_fn_t cbfunc, void \\\\*cbdata) with gil:/pmix_event_notification_cbfunc_fn_t cbfunc, void \\\\*cbdata) nogil:/g\" /opt/hpc/local/build/openpmix/bindings/python/pmix.pyx";
#     print_it=0 
# }
# ' "$SCRIPT_PATH" > tmp_script.sh && mv tmp_script.sh "$SCRIPT_PATH"

chmod +x "$SCRIPT_PATH"
sudo ./install_docker.sh $1