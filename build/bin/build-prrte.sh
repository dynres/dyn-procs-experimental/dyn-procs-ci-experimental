#!/bin/bash -xe

./autogen.pl

source_dir=$PWD

echo "building PRRTE from ${source_dir}"
rm -rf ../prrte-build
mkdir  ../prrte-build
cd     ../prrte-build

${source_dir}/configure --prefix=${PRRTE_ROOT} \
            --with-hwloc=${HWLOC_INSTALL_PATH} \
            --with-libevent=${LIBEVENT_INSTALL_PATH} \
            --with-pmix=${PMIX_ROOT} \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
