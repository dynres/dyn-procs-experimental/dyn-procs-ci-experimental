#!/bin/bash -xe

./autogen.pl

source_dir=$PWD

echo "building Open MPI from ${source_dir}"
rm -rf ../ompi-build
mkdir  ../ompi-build
cd     ../ompi-build

${source_dir}/configure --prefix=${OMPI_ROOT} \
            --with-hwloc=${HWLOC_INSTALL_PATH} \
            --with-libevent=${LIBEVENT_INSTALL_PATH} \
            --with-pmix=${PMIX_ROOT} \
            --with-slurm=yes \
            --enable-mpirun-prefix-by-default \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
