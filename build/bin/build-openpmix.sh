#!/bin/bash -xe

./autogen.pl

source_dir=$PWD

echo "building Open PMIx from ${source_dir}"
rm -rf ../openpmix-build
mkdir  ../openpmix-build
cd     ../openpmix-build

${source_dir}/configure --prefix=${PMIX_ROOT} \
            --with-hwloc=${HWLOC_INSTALL_PATH} \
            --with-libevent=${LIBEVENT_INSTALL_PATH} \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
