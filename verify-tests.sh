#!/bin/bash

total_tests=0
successful_tests=0

declare -a started_tests

file="./build/test_applications/results.log"

while IFS= read -r line
do
  if [[ $line == RUNNING* ]]; then
    ((total_tests++))
    started_tests+=("$line")
  fi

  if [[ $line == *"finished benchmark suc"* ]] ; then
    ((successful_tests++))
    unset 'started_tests[${#started_tests[@]}-1]'
  fi
done < "$file"

echo "> Test results: [$successful_tests/$total_tests]"

if [[ ${#started_tests[@]} -gt 0 ]]; then
  echo "> Failed tests: ${#started_tests[@]}"
  for test in "${started_tests[@]}"; do
    echo "-- $test"
  done
fi

if [[ $total_tests -eq 0 ]]; then
  failed_tests=42
else
  failed_tests=$((total_tests - successful_tests))
fi

exit "$failed_tests"
