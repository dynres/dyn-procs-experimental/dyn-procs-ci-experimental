Use 'git apply <path-to-patch>' to apply these patches.

The patch: 
enable_pmixv5.patch
has been tested with Slurm upstream at:

commit 15e256e6673239dc503c9efa61c7abd005328ac0 (origin/master, origin/HEAD)
Author: Marcin Stolarek <cinek@schedmd.com>
Date:   Fri Dec 3 14:17:29 2021 +0000

    scontrol - accept 'delete <ENTITY> <ID>' format besides '<ENTITY>=<ID>'
    
    This is to support space in place where = was required before.
    
    Bug 12956

