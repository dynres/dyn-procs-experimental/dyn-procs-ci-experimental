#!/bin/bash

if grep -q "ID=ubuntu" /etc/os-release; then
    mkdir -p -m0755 /var/run/sshd
else
    mkdir -p /run/sshd
fi

/usr/sbin/sshd -D