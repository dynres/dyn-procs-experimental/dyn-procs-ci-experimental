#!/bin/bash 

############################################################################
# TODO: 
#       If tests are verified, save/publish the images.
#       ! Caution about the mounted volumes 
#       Image name format: 
#           docker_image_rocky-linux9_gcc_hwloc5.0_libevent-3.3.3_commit-xyz
############################################################################

# Docker Troubleshooting Block
#
# This block offers solutions to potential Docker-related issues.
# It suggests flushing changes and restarting the Docker daemon as a primary troubleshooting step.
#
# In case of docker related issues, try the following:
# Flush changes and restart Docker
# sudo systemctl daemon-reload
# sudo systemctl restart docker

# Test Images Definition Block
#
# This block defines a primary working test image and provides space for alternative images.
# The `test_image` variable is a placeholder for the default image to be used in the script.
#
# Working test images:
test_image="rockylinux:9 gcc"
# test_image="opensuse/tumbleweed gcc"
##############################################

# test_image="ubuntu:latest gcc" # mpi installed but not found in test apps, CPATH not set
# test_image="ubuntu:latest intel"

# test_image="rockylinux:9 llvm" # https://dragonegg.llvm.org/
# test_image="opensuse/leap gcc" # libevent issue
# test_image="rockylinux:8 gcc" # no go: gcc max 8.5, min 10 needed. can be installed from source but takes hours to build
node_count=4 # min 4
hwloc_version="2.9.2"
libevent_version="2.1.12"
TEST_SCRIPT="dyn_mpi_sessions_test_all.sh"
# TEST_SCRIPT="dyn_mpi_sessions_test_all_8nodes.sh"
BUILD=true
INSTALL_PROJECT=""
RUN_TESTS=true

display_help()
{
    echo ""
    echo "Usage:"
    echo "  command [OPTIONS]"
    echo "Options:"
    echo "  -t, --tag               Test image. Default: "rockylinux:9 gcc""
    echo "  -o, --os                OS."
    echo "  -c, --compiler          Compiler."
    echo "  -n, --node              Node count."
    echo "  -h, --hwloc             HWLOC version."
    echo "  -l, --libevent          Libevent version."
    echo "  -s, --test-script       Test script."
    echo "  -b, --build             Build."
    echo "  -p, --install-project   Install project."
    echo "  -r, --run-tests         Run tests."
    echo ""
    exit 1 
}

tag_used=false
os_used=false
compiler_used=false
SHORT=t:o:c:n:h:l:s:b:p:r:
LONG=tag:,os:,compiler:,node:,hwloc:,libevent:,test-script:,build:,install-project:,run-tests:

PARSED=$(getopt --options=$SHORT --longoptions=$LONG --name "$0" -- "$@" 2>/dev/null)

if [ $? -ne 0 ]; then
    display_help
fi

eval set -- "$PARSED"

while true ; do
    case "$1" in
        -t|--tag)
            test_image="$2"
            tag_used=true
            shift 2
            ;;
        -o|--os)
            os="$2"
            os_used=true
            shift 2
            ;;
        -c|--compiler)
            compiler="$2"
            compiler_used=true
            shift 2
            ;;
        -n|--node)
            node_count="$2"
            if [ "$node_count" -lt 4 ]; then
                echo "Error: Minimum node count is 4."
                exit 1
            fi
            shift 2
            ;;
        -h|--hwloc)
            hwloc_version="$2"
            shift 2
            ;;
        -l|--libevent)
            libevent_version="$2"
            shift 2
            ;;
        -s|--test-script)
            TEST_SCRIPT="$2"
            shift 2
            ;;
        -b|--build)
            BUILD="$2"
            shift 2
            ;;
        -p|--install-project)
            INSTALL_PROJECT="$2"
            shift 2
            ;;
        -r|--run-tests)
            RUN_TESTS="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
    esac
done

if $tag_used && ($os_used || $compiler_used); then
    echo "Error: Please use either --tag or (--os and --compiler), but not both."
    exit 1
fi

if $os_used && $compiler_used; then
    test_image="$os $compiler"
fi

echo "=============================================================================="
echo "> Starting the test for the $(echo "$test_image" | sed 's/ / + /') with $node_count nodes!"
echo "=============================================================================="

# cleanup Function
#
# This function initiates a cleanup process for system resources and processes.
# The steps involved include:
# 1. Logging the commencement of the cleanup process.
# 2. Terminating a process identified by its PID if it's still running.
# 3. Executing a custom shutdown script for an Ubuntu PC.
# 4. Removing all stopped Docker containers.
# 5. Deleting any dangling Docker images (i.e., untagged images).
# 6. Forcing the Docker host to leave any Docker Swarm cluster it might be a part of.
#
# Assumption:
# - A global variable $PID should have been set prior to invoking this function, representing the process ID you wish to terminate.
# - `./tmp/shutdown-ubuntu-PC.sh` is a valid and executable script path.
# - The user invoking this function should have necessary permissions to execute Docker commands and process termination.
#
# Usage:
# cleanup
#
cleanup() {
    echo "> Starting cleanup..."
    if kill -0 $PID 2>/dev/null; then
        kill $PID
    fi 
    # ./tmp/shutdown-ubuntu-PC.sh 
    docker kill $(docker ps -aq) || true
    docker image prune -f || true
    docker swarm leave --force 
}

trap cleanup EXIT

if ! command -v jq &> /dev/null
then
    echo "> jq utility not found, installing..."
    sudo apt-get install jq -y
fi

if ! command -v sponge &> /dev/null
then
    echo "> sponge not found, installing..."
    sudo apt-get install moreutils -y
fi

# download_hwloc Function
#
# This function is designed to download a specified version of the `hwloc` software package.
# The `hwloc` package provides a portable abstraction of the hierarchical topology of modern architectures.
# This function ensures that:
# 1. The version specified for download meets or exceeds the defined minimum supported version.
# 2. If the specified version already exists in the target directory, it will not be redownloaded.
# 3. Older versions of `hwloc` in the target directory will be removed before the new version is downloaded.
# 4. The download source URL is based on the official `open-mpi` release location.
#
# Parameters:
# - $1 (version): The version of `hwloc` you want to download (e.g., "2.5.1").
# - $2 (install_dir): The directory where the `hwloc` package should be downloaded.
#
# Usage:
# download_hwloc "2.5.1" "/path/to/directory"
#
download_hwloc() {
    local version="$1"
    local install_dir="$2"
    local min_version="2.5" 
    local filename="hwloc-${version}.tar.gz"

    if [ "$(printf '%s\n' "$version" "$min_version" | sort -V | head -n1)" != "$min_version" ]; then
        echo "> Error: hwloc version $version is not supported. Minimum supported version is $min_version."
    else
        if [ -f "${install_dir}/${filename}" ]; then
            echo "> hwloc $version already exists in $install_dir. Continuing..."
        else
            if ls ${install_dir}/hwloc-*.tar.gz 1> /dev/null 2>&1; then
                echo "> Removing older versions of hwloc in $install_dir..."
                rm -f ${install_dir}/hwloc-*.tar.gz
            fi
            echo "> Downloading hwloc $version to $install_dir..."
            wget -P $install_dir https://download.open-mpi.org/release/hwloc/v${version%.*}/hwloc-${version}.tar.gz
        fi
    fi
}

# download_libevent Function
#
# This function is crafted to download a designated version of the `libevent` software library.
# `libevent` is an asynchronous event notification library, widely used for crafting scalable event-driven applications.
# When invoked, this function will:
# 1. Validate that the user-specified version is equal to or greater than the minimum accepted version.
# 2. Check the target directory for the presence of the intended version. If it exists, re-download is skipped.
# 3. Clear out any older versions of `libevent` found in the target directory prior to downloading the desired version.
# 4. Fetch the library from the official GitHub `libevent` releases.
#
# Parameters:
# - $1 (version): The version of `libevent` you intend to download (e.g., "2.1.12-stable").
# - $2 (install_dir): The directory wherein the `libevent` library will be stored post-download.
#
# Usage:
# download_libevent "2.1.12-stable" "/desired/download/path"
#
download_libevent() {
    local version="$1"
    local install_dir="$2"
    local min_version="2.1.12"
    local filename="libevent-${version}-stable.tar.gz"

    if [ "$(printf '%s\n' "$version" "$min_version" | sort -V | head -n1)" != "$min_version" ]; then
        echo "> Error: libevent version $version is not supported. Minimum supported version is $min_version."
    else
        if [ -f "${install_dir}/${filename}" ]; then
            echo "> libevent $version already exists in $install_dir. Continuing..."
        else
            if ls ${install_dir}/libevent-*-stable.tar.gz 1> /dev/null 2>&1; then
                echo "> Removing older versions of libevent in $install_dir..."
                rm -f ${install_dir}/libevent-*-stable.tar.gz
            fi
            echo "> Downloading libevent $version to $install_dir..."
            wget -P $install_dir https://github.com/libevent/libevent/releases/download/release-$version-stable/libevent-$version-stable.tar.gz
        fi
    fi
}

download_hwloc $hwloc_version ./src
download_libevent $libevent_version ./src

# Extract the base image and compiler details from the given `$test_image` string.
# `$test_image` should have the format: "base_image compiler".
# This also formats the docker image name to be used later in the script.
base_image=$(echo "$test_image" | cut -d ' ' -f1)
compiler=$(echo "$test_image" | cut -d ' ' -f2)
base_image_modified=$(echo "$base_image" | tr '/:' '-')
docker_image_name="${base_image_modified}-${compiler}"

# Builds the Docker image using the parsed base image and compiler.
# If the build is successful, it logs the success message, otherwise, logs the failure and exits.
# This block is only executed if `$BUILD` is set to "true".
if [[ "$BUILD" == "true" ]]; then
    if ./docker-build.sh $base_image $compiler ; then
        echo "Build succeeded for $base_image + $compiler!"
    else
        echo "Build failed for $base_image + $compiler!"
        exit 1
    fi
fi

# Clones a specific Git repository into the `./build/dyn_procs_setup` directory.
# If the clone operation is successful, it logs the success message, otherwise, logs the error and exits.
# This block only attempts to clone if the directory doesn't already exist.
if [ ! -d "./build/dyn_procs_setup" ]; then
    if git clone -b docker_setup https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup.git "./build/dyn_procs_setup"; then
        echo "Clone successful."
    else
        echo "Error: Failed to clone the Git repository."
        exit 1
    fi
fi

# Logs a message and starts a Docker cluster with the previously defined docker image name.
# The number of nodes in the cluster is determined by `$node_count`.
echo "Starting docker-cluster:$docker_image_name with $node_count nodes..."
./start.sh -n $node_count -i "$docker_image_name"

# Function: docker_install
#
# Executes a command inside the Docker container to install a specified project.
# It drops into the 'n01' Docker container as 'mpiuser' and runs the project's installation script.
#
# Parameters:
# - $1 (project): The name of the project to be installed.
docker_install() {
    project=$1

    echo "==============================================================================="
    echo "> Dropping into the n01 container as mpiuser and installing $project..."
    echo "==============================================================================="

    docker exec -u mpiuser n01 /opt/hpc/local/build/install_project.sh $project
    if [ $? -ne 0 ]; then
        echo "Failed to install the project. Exiting..."
    exit 1
fi
}	

# Function: install_project
#
# This function installs a specified project and its dependencies inside a Docker container.
# It utilizes a recursive approach to first install dependencies before installing the main project.
# The dependencies for each project are defined in an associative array.
#
# Parameters:
# - $1 (project): The name of the project to be installed.
install_project() {
    project=$1
    # Define the dependency chains in a declare associative array
    declare -A dependencies
    dependencies=(["openpmix"]="" ["prrte"]="openpmix" ["ompi"]="prrte" ["tests"]="ompi" ["dyn_rm"]="" ["libpfasst"]="")

    # Recursively install dependencies
    if [[ ${dependencies[$project]} != "" ]]; then
        install_project ${dependencies[$project]}
    fi

    docker_install $project
}


shopt -s nocasematch  # Set shell option to ignore case in string comparisons
# Decides which projects to install based on the `$INSTALL_PROJECT` variable.
# If the value is "none" or "skip", no installation occurs.
# If a specific project is named, that project and its dependencies are installed.
# If the value is "all" or empty, all projects are installed.
if [[ "$INSTALL_PROJECT" == "none" || "$INSTALL_PROJECT" == "skip" ]]; then
    echo "Project installation skipped as per user request."
    # Continue with the rest of the script
else
    if [[ "$INSTALL_PROJECT" != "" ]]; then
        # If a specific project is specified, install it and its dependencies
        if [[ "$INSTALL_PROJECT" == "all" ]]; then
            docker_install "all"
        else
            install_project "$INSTALL_PROJECT"
        fi
    else
        # If no specific project is specified, install all projects
        docker_install "all"
    fi
fi

shopt -u nocasematch  # Unset shell option to ignore case in string comparisons

# Test Execution Block
#
# This block is responsible for running the specified tests if the "RUN_TESTS" flag is set to "true".
# It consists of the following key steps:
# 1. Changing to the current working directory.
# 2. Printing a banner to indicate the start of the testing phase.
# 3. Performing the necessary setup and adjustments for various environments (e.g., opensuse/tumbleweed, rockylinux:9).
# 4. Executing the specified test script inside the Docker container, adjusting the path to the 'prterun' executable as needed.
# 5. Capturing and logging the results.
# 6. Running the 'verify-tests.sh' script to assess the tests' outcomes.
# 7. Updating the "EXIT_STATUS" variable based on the verification results.
#
EXIT_STATUS=0
if [[ "$RUN_TESTS" == "true" ]]; then
    pwd
    echo ""
    echo "==============================================================================="
    echo "> Starting to test..."
    echo "==============================================================================="
    time (
      docker exec -u mpiuser n01 bash -c "cd /opt/hpc/build/test_applications &&\
                                          source /opt/hpc/build/dyn_procs_setup/env_vars_docker.sh &&\
                                          sudo sed -i '/^\/opt\/hpc\/install\/prrte\/bin\/prterun/!s|prterun|/opt/hpc/install/prrte/bin/prterun|' ./${TEST_SCRIPT} &&\
                                          ./${TEST_SCRIPT} | sudo tee results.log 2>&1"
    )  
    echo -e "\n\n"
    echo "==============================================================================="
    echo "                              TESTS COMPLETED!"
    sudo ./verify-tests.sh | sudo tee ./artifacts/results/output.log 
    EXIT_STATUS=$?
    echo "==============================================================================="
fi

# update_build_results Function
#
# This function is designed to record the results of a build operation into a structured JSON file (`results.json`).
# It captures the outcome of builds against various combinations of OS base image, compiler, and software library versions.
#
# The purpose is to give a snapshot of build successes or failures for a variety of configurations.
# Given the complexity of software builds that depend on numerous factors, this function helps maintain a record 
# for quick reference, especially useful for regression testing and build environment troubleshooting.
#
# When invoked, this function will:
# 1. Determine the result descriptor based on the exit status (0 translates to "successful build", non-zero to "failed build").
# 2. Ensure the existence of the base image key in the JSON structure. If not present, it initializes it.
# 3. Navigate deeper to check/set the compiler key under the base image.
# 4. Dive further to account for the version of `hwloc` used.
# 5. Finally, record the version of `libevent` and the result of the build.
# 6. Write the updated data structure back to `results.json`.
#
# Parameters:
# - $1 (base_image): Denotes the base image (like "rockylinux:9") against which the build took place.
# - $2 (compiler): Represents the compiler employed for the build (for example, "gcc").
# - $3 (EXIT_STATUS): An integer flag indicating the build outcome. 0 stands for success, while non-zero indicates failure.
# - $4 (hwloc_version): Specifies the `hwloc` library version used during the build (e.g., "2.9.2").
# - $5 (libevent_version): Notes the `libevent` library version engaged in the build (e.g., "2.1.12").
#
# Usage:
# update_build_results "rockylinux:9" "gcc" 0 "2.9.2" "2.1.12"
#
update_build_results() {
    if [[ "$EXIT_STATUS" -eq 0 ]]; then
        result="successful build"
    else
        result="failed build"
    fi

    # Update results.json
    jq --arg os "$base_image" \
       --arg compiler "$compiler" \
       --arg hwloc "$hwloc_version" \
       --arg libevent "$libevent_version" \
       --arg result "$result" \
       '.[$os] = (.[$os] // {}) | 
        .[$os][$compiler] = (.[$os][$compiler] // {}) | 
        .[$os][$compiler]["hwloc"] = (.[$os][$compiler]["hwloc"] // {}) | 
        .[$os][$compiler]["hwloc"][$hwloc] = (.[$os][$compiler]["hwloc"][$hwloc] // {}) | 
        .[$os][$compiler]["hwloc"][$hwloc]["libevent"] = (.[$os][$compiler]["hwloc"][$hwloc]["libevent"] // {}) | 
        .[$os][$compiler]["hwloc"][$hwloc]["libevent"][$libevent] = $result' results.json > tmp.$$.json && mv tmp.$$.json results.json
}

# If the results.json file doesn't exist or is empty, create it with an empty object.
if [[ ! -f results.json ]] || [[ ! -s results.json ]]; then
  echo '{}' > results.json
fi

update_build_results "$base_image" "$compiler" "$EXIT_STATUS" "$hwloc_version" "$libevent_version"
results.json tee -a ./artifacts/results/output.log
exit "$EXIT_STATUS"
