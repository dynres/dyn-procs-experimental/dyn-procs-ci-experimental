# test_image="opensuse/tumbleweed gcc"
test_image="rockylinux:9 gcc"
# test_image="ubuntu:latest intel"

node_count=1 # min 4

base_image=$(echo "$test_image" | cut -d ' ' -f1)
compiler=$(echo "$test_image" | cut -d ' ' -f2)


base_image_modified=$(echo "$base_image" | tr '/:' '-')
docker_image_name="${base_image_modified}-${compiler}"
echo "Starting docker-cluster:$docker_image_name with $node_count nodes..."
./start.sh -n $node_count -i "$docker_image_name"

# docker exec -it -u root -w  /opt/hpc/build/ --env COLUMNS=`tput cols` --env LINES=`tput lines` n01 bash
docker exec -it -u mpiuser -w  /opt/hpc/build/ --env COLUMNS=`tput cols` --env LINES=`tput lines` n01 bash
        