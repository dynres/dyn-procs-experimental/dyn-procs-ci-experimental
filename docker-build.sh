#!/bin/bash

set -e

check_preconditions() {
    if [ "$#" -ne 2 ]; then
        echo "Usage: $0 <os_image> <compiler>"
        exit 1
    fi

    if ! dpkg -s net-tools &> /dev/null; then
        echo "net-tools package is not installed. Installing..."
        sudo apt-get update
        sudo apt-get install -y net-tools
    fi
}

check_preconditions "$@"

ipv4_address=$(ifconfig | grep -oP 'inet \K[\d.]+' | head -n 1)

echo "Host IP: $ipv4_address"
echo "==============================================================================="
echo "> Initializing the swarm..."

is_part_of_swarm=$(docker info --format '{{.Swarm.LocalNodeState}}')
if [ "$is_part_of_swarm" != "active" ]; then
    docker swarm init --advertise-addr "$ipv4_address"
else
    echo "Node is already part of a swarm. Skipping swarm initialization..."
fi
echo "==============================================================================="

os_image="$1"
compiler="$2"
image_tag=$(echo "$os_image-$compiler" | sed -e 's/\//-/g' -e 's/:/-/g')

echo "[DEBUG] os_image: $os_image | compiler: $compiler | image_tag: $image_tag"

if [[ "$compiler" == "intel" ]] && [[ "$os_image" == *"ubuntu"* ]]; then
    echo "> Using ubuntu:latest..."
    os_image="intel/oneapi-hpckit:latest"
    image_tag="ubuntu-latest-intel"
fi

image_name="docker-cluster:$image_tag"

echo "==============================================================================="
echo "> Starting the build for the $image_tag image!"
echo "==============================================================================="

docker build -t $image_name -f Dockerfile.slurm --build-arg BASE_IMAGE=$os_image --build-arg COMPILER=$compiler . 2>&1 | tee error.log

build_status=$? # Capture the exit status of the docker build command

if [ $build_status -ne 0 ]; then
  echo ">>> Docker build failed with exit status $build_status <<<"
  exit 1
fi

docker image prune -f

exit 0
